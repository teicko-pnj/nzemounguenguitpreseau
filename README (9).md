# TP4 : DHCP

## I. DHCP Client

🌞 Déterminer

    PS C:\Windows\system32> ipconfig /all

    Configuration IP de Windows

    Carte Ethernet VMware Network Adapter VMnet1 :

    Carte réseau sans fil Wi-Fi :

    Suffixe DNS propre à la connexion. . . :
    Description. . . . . . . . . . . . . . : Intel(R) Wi-Fi 6E AX211 160MHz
    Adresse physique . . . . . . . . . . . : E4-0D-36-06-76-60
    DHCP activé. . . . . . . . . . . . . . : Oui
    Configuration automatique activée. . . : Oui
    Adresse IPv6 de liaison locale. . . . .: fe80::d807:ee16:b687:3d08%9(préféré)
    Adresse IPv4. . . . . . . . . . . . . .: 10.33.72.140(préféré)
    Masque de sous-réseau. . . . . . . . . : 255.255.240.0
    Bail obtenu. . . . . . . . . . . . . . : vendredi 27 octobre 2023 13:57:12 🌞
    Bail expirant. . . . . . . . . . . . . : samedi 28 octobre 2023 09:14:21 🌞
    Passerelle par défaut. . . . . . . . . : 10.33.79.254
    Serveur DHCP . . . . . . . . . . . . . : 10.33.79.254 🌞
    IAID DHCPv6 . . . . . . . . . . . : 132386102
    DUID de client DHCPv6. . . . . . . . : 00-01-00-01-2C-82-50-96-BC-EC-A0-11-AD-7B
    Serveurs DNS. . .  . . . . . . . . . . : 8.8.8.8
                                       1.1.1.1
    NetBIOS sur Tcpip. . . . . . . . . . . : Activé

🌞 Capturer un échange DHCP

🌞 Analyser la capture Wireshark

    2	1.100502	10.33.79.254	10.33.72.140	DHCP	342	DHCP Offer    - Transaction ID 0x2e376166
#
    Dynamic Host Configuration Protocol (Offer)
    Message type: Boot Reply (2)
    Hardware type: Ethernet (0x01)
    Hardware address length: 6
    Hops: 0
    Transaction ID: 0x2e376166
    Seconds elapsed: 0
    Bootp flags: 0x0000 (Unicast)
    Client IP address: 0.0.0.0
    Your (client) IP address: 10.33.72.140
    Next server IP address: 0.0.0.0
    Relay agent IP address: 0.0.0.0
    Client MAC address: IntelCor_06:76:60 (e4:0d:36:06:76:60)
    Client hardware address padding: 00000000000000000000
    Server host name not given
    Boot file name not given
    Magic cookie: DHCP
    Option: (53) DHCP Message Type (Offer)
    Option: (54) DHCP Server Identifier (10.33.79.254)
    Option: (51) IP Address Lease Time
    Option: (1) Subnet Mask (255.255.240.0)
    Option: (3) Router
    Option: (6) Domain Name Server
    Option: (255) End
    Padding: 00000000000000000000000000000000000000000000

## II. Serveur DHCP

### 1. Topologie

### 2. Tableau d'adressage

### 3. Setup topologie

🌞 Preuve de mise en place

    DHCP : 
    [yanice@localhost ~]$ ping google.com
    PING google.com (172.217.18.206) 56(84) bytes of data.
    64 bytes from par10s38-in-f14.1e100.net (172.217.18.206): icmp_seq=1 ttl=115 time=18.2 ms
    64 bytes from ham02s14-in-f206.1e100.net (172.217.18.206): icmp_seq=2 ttl=115 time=19.3 ms
    64 bytes from par10s38-in-f14.1e100.net (172.217.18.206): icmp_seq=3 ttl=115 time=22.4 ms
    ^C
    --- google.com ping statistics ---
    3 packets transmitted, 3 received, 0% packet loss, time 2004ms
    rtt min/avg/max/mdev = 18.229/19.997/22.435/1.781 ms
#
    node2 : 
    [yanice@localhost ~]$ ping google.com
    PING google.com (172.217.18.206) 56(84) bytes of data.
    64 bytes from par10s38-in-f14.1e100.net (172.217.18.206): icmp_seq=1 ttl=115 time=17.8 ms
    64 bytes from par10s38-in-f14.1e100.net (172.217.18.206): icmp_seq=2 ttl=115 time=22.5 ms
    64 bytes from par10s38-in-f14.1e100.net (172.217.18.206): icmp_seq=3 ttl=115 time=27.6 ms
    ^C64 bytes from 172.217.18.206: icmp_seq=4 ttl=115 time=37.5 ms

    --- google.com ping statistics ---
    4 packets transmitted, 4 received, 0% packet loss, time 15164ms
    rtt min/avg/max/mdev = 17.774/26.334/37.455/7.294 ms
#
    node2 : 
    [yanice@localhost ~]$ traceroute 172.217.18.206
    traceroute to 172.217.18.206 (172.217.18.206), 30 hops max, 60 byte packets
    1  _gateway (10.4.1.254)  1.209 ms  1.185 ms  0.931 ms
    2  10.0.3.2 (10.0.3.2)  0.929 ms  1.306 ms  1.304 ms
    3  10.0.3.2 (10.0.3.2)  4.709 ms  4.332 ms  3.433 ms

### 4. Serveur DHCP

🌞 Rendu

    [yanice@localhost ~]$ sudo dnf -y install dhcp-server

    [yanice@localhost ~]$ sudo nano /etc/dhcp/dhcpd.conf

    option domain-name     "srv.world";
    option domain-name-servers     dlp.srv.world;
    default-lease-time 600;
    max-lease-time 7200;
    authoritative;
    subnet 10.4.1.0 netmask 255.255.255.0 {
    range dynamic-bootp 10.4.1.200 10.4.1.254;
    option broadcast-address 10.4.1.255;
    option routers 10.4.1.254;
    }

    [yanice@localhost ~]$ sudo systemctl enable --now dhcpd

    [yanice@localhost ~]$ sudo firewall-cmd --add-service=dhcp

    [yanice@localhost ~]$ sudo firewall-cmd --runtime-to-permanent
#
    [yanice@localhost ~]$ sudo systemctl status dhcpd
    ● dhcpd.service - DHCPv4 Server Daemon
     Loaded: loaded (/usr/lib/systemd/system/dhcpd.service; enabled; preset: disabled)
     Active: active 🌞 (running) since Fri 2023-10-27 15:41:29 CEST; 5min ago
       Docs: man:dhcpd(8)
             man:dhcpd.conf(5)
    Main PID: 1716 (dhcpd)
     Status: "Dispatching packets..."
      Tasks: 1 (limit: 4610)
     Memory: 5.2M
        CPU: 11ms
     CGroup: /system.slice/dhcpd.service
             └─1716 /usr/sbin/dhcpd -f -cf /etc/dhcp/dhcpd.conf -user dhcpd -group dhcpd --no-pid

    Oct 27 15:41:29 localhost.localdomain dhcpd[1716]: Config file: /etc/dhcp/dhcpd.conf
    Oct 27 15:41:29 localhost.localdomain dhcpd[1716]: Database file: /var/lib/dhcpd/dhcpd.leases
    Oct 27 15:41:29 localhost.localdomain dhcpd[1716]: PID file: /var/run/dhcpd.pid
    Oct 27 15:41:29 localhost.localdomain dhcpd[1716]: Source compiled to use binary-leases
    Oct 27 15:41:29 localhost.localdomain dhcpd[1716]: Wrote 0 leases to leases file.
    Oct 27 15:41:29 localhost.localdomain dhcpd[1716]: Listening on LPF/enp0s3/08:00:27:b8:0c:4b/10.4.1.0/24
    Oct 27 15:41:29 localhost.localdomain dhcpd[1716]: Sending on   LPF/enp0s3/08:00:27:b8:0c:4b/10.4.1.0/24
    Oct 27 15:41:29 localhost.localdomain dhcpd[1716]: Sending on   Socket/fallback/fallback-net
    Oct 27 15:41:29 localhost.localdomain dhcpd[1716]: Server starting service.
    Oct 27 15:41:29 localhost.localdomain systemd[1]: Started DHCPv4 Server Daemon.
#
    [yanice@localhost ~]$ sudo cat /etc/dhcp/dhcpd.conf
    option domain-name     "srv.world";
    option domain-name-servers     dlp.srv.world;
    default-lease-time 600;
    max-lease-time 7200;
    authoritative;
    subnet 10.4.1.0 netmask 255.255.255.0 {
    range dynamic-bootp 10.4.1.137 10.4.1.237;
    option broadcast-address 10.4.1.255;
    option routers 10.4.1.254;
    }

### 5. Client DHCP

🌞 Test !

🌞 Prouvez que

🌞 Bail DHCP serveur

### 6. Options DHCP

🌞 Nouvelle conf !

🌞 Test !

🌞 Capture Wireshark